const Services = require('../Services');

//mensaje de error
const { InternalError } = require('../settings');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView } = require('./index');

async function View(job, done){

    try{

        const { } = job.data;

        console.log(job.id);

        let { statuscode, data, message } = await Services.View({  });
        
        const newData = data.map(v => (v.worker = job.id, v));
        
        done(null, { statuscode, data: newData, message });


    }catch(error){
        console.log({ step: 'Adapter queueView', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
};

async function Create(job, done){

    try{

        const { age, color, name } = job.data;

        let { statuscode, data, message } = await Services.Create({ age, color, name });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueCreate', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
};

async function Delete(job, done){

    try{

        const { id } = job.data;

        let { statuscode, data, message } = await Services.Delete({ id });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueDelete', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
};

async function FindOne(job, done){

    try{

        const { id } = job.data;

        let { statuscode, data, message } = await Services.FindOne({ id });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueFindOne', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
};

async function Update(job, done){

    try{

        const { age, color, name, id } = job.data;

        let { statuscode, data, message } = await Services.Update({ age, color, name, id });
        
        done(null, { statuscode, data, message });

    }catch(error){
        console.log({ step: 'Adapter queueUpdate', error: error.toString() });
        
        done(null, { statuscode: 500, message: InternalError } );
    }
    
};

async function run(){
    try{

        console.log('Vamos a iniciar Worker');


        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueFindOne.process(FindOne);

        queueUpdate.process(Update);
        

    }catch(error){
        console.log('Catch' + error);
    }
}

module.exports = { 
    View,
    Create,
    Delete,
    FindOne,
    Update,
    run
}