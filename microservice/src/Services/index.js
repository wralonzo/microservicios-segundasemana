const Controller = require('../Controllers');
const { InternalError } = require('../settings');

async function Create ({ name, age, color }) {
    try {

        let { statuscode, data, message } = await Controller.Create({ name, age, color  });
    
         return { statuscode, data, message  }

    } catch (error) {

        console.log({step: 'Servicio Create', error: error.toString()});
        
        return { statuscode: 500, message: error.toString()};
    }
}
async function Delete ({ id }) {
    try {

        const findOne =  await Controller.FindOne({ where: { id } })

        if(findOne.statuscode !== 200){
        //     let response = {
        //         400: {statuscode: 400, message: 'No existe el usuario a eliminar' },
        //         500: {statuscode: 500, message: InternalError }
        //     };

        //     return response(findOne.statuscode);

            switch(findOne.statuscode) {

                case 400: return { statuscode: 400, message: 'No existe el usuario a eliminar' };
                
                case 500: return { statuscode: 500, message: InternalError };

                default: return { statuscode: findOne.statuscode, message: findOne.message }

            }
        }

        const del =   await Controller.Delete({ where: { id } });
    
        if (del.statuscode === 200) return { statuscode: 200, data: findOne.data };

         return { statuscode: 400, message:  InternalError }

    } catch (error) {
        console.log({step: 'Servicio Delete', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function Update ({ name, age, color, id }) {
    try {

        let { statuscode, data, message } = await Controller.Update({ name, age, color, id });
    
         return { statuscode, data, message  }

    } catch (error) {
        console.log({step: 'Servicio Update', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function FindOne ({ id }) {
    try {

        let { statuscode, data, message } = await Controller.FindOne({ where: { id } });
    
         return { statuscode, data, message  }

    } catch (error) {

        console.log({step: 'Servicio Find one', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
async function View ({  }) {
    try {

        let { statuscode, data, message } = await Controller.View({  });
    
         return { statuscode, data, message  }

    } catch (error) {
        console.log({step: 'Servicio View', error: error.toString()});

        return { statuscode: 500, message: error.toString()};
    }
}
module.exports = { Create, Delete, Update, FindOne, View }