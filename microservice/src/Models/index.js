const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define(
    'curso', {
        name: {
            type: DataTypes.STRING
        },
        age: {
            type: DataTypes.BIGINT
        },
        color: {
            type: DataTypes.STRING
        }
    }
);

async function SyncDB() {
    try{

        console.log('Vamos a iniciar la base de datos del sistema');

        await Model.sync({ logging: false });

        console.log('La base de datos del sistema iniciada');

        return { statuscode: 200, data: 'Ok' };

    }
    catch(error){
        console.log('Model: index.js');
        console.log(error);
        
        return { statuscode: 500, message: error.toString() };
    }
}

module.exports = { Model, SyncDB }