
import React, { useEffect, useState}  from 'react';

const { io }  = require('socket.io-client');



function App() {

	const [ id , setId ] = useState();

	const [ data, setData ] = useState([]);


	useEffect(() => {

		const socket = io('http://localhost', { 
			transports: ['websocket'],
			jsonp: false,
		});

		setTimeout(() => setId(socket.id), 200);

		socket.on('res:microservice:view', ({ statuscode, data, message }) => {

            console.log('res:microservice:view', { statuscode, data, message });
            
			console.log({ statuscode, data, message });

			if(statuscode === 200) setData( data );
       
		});
        
        socket.on('res:microservice:create', ({ statuscode, data, message }) => {

            console.log('res:microservice:create', { statuscode, data, message });
        });

        socket.on('res:microservice:delete', ({ statuscode, data, message }) => {

            console.log('res:microservice:delete', { statuscode, data, message });
        });

        socket.on('res:microservice:update', ({ statuscode, data, message }) => {

            console.log('res:microservice:update', { statuscode, data, message });
        });

        socket.on('res:microservice:findOne', ({ statuscode, data, message }) => {

            console.log('res:microservice:findOne', { statuscode, data, message });
        });

		setInterval(() => { socket.emit('req:microservice:view', ({ })) }, 15000);

	}, []);
  return (
    <div>

		<header>
			<h1>{id ? `Estas en linea ${id}` : 'Fuera de linea'}</h1>

			{data.map((response, key) => {
				return <p key={key}>Nombre: {response.name}, Edad: {response.age} {response.worker}</p>
			})}
        </header>



    </div>
  );
}

export default App;
