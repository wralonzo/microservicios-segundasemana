
//importar librerias
const bull = require('bull');

const redis = { host: 'localhost', port: 6379 }

const opts = { redis: { host: redis.host, port: redis.port } }

//constante para poder integrar varios microservicions
const queueCreate = bull('curso:create', opts);

const queueDelete = bull('curso:delete', opts);

const queueUpdate = bull('curso:update', opts);

const queueFindOne = bull('curso:findOne', opts);

const queueView = bull('curso:view', opts);

async function Create({ name, age, color }) {

    try{

        const job =  await  queueCreate.add( { name, age, color } );  

        const { statuscode, data, message } = await job.finished(); 

        return { statuscode, data, message }

    }catch(error){

        console.log(error)

    }

}
async function Delete({ id }) {

    try{

        const job =  await  queueDelete.add({ id });  

        const { statuscode, data, message } = await job.finished(); 

        return { statuscode, data, message }


    }catch(error){

        console.log(error)

    }

}
async function Update({ name, age, color, id }) {

    try{

        const job =  await  queueUpdate.add({ name, age, color, id });  

        const { statuscode, data, message } = await job.finished(); 
        

       return  { statuscode, data, message }


    }catch(error){

        console.log(error)

    }

}
async function FindOne({ id }) {

    try{

        const job =  await  queueFindOne.add({ id });  

        const { statuscode, data, message } = await job.finished(); 
        
        console.log({ statuscode, data, message });

        return { statuscode, data, message }

    }catch(error){

        console.log(error)

    }

}
async function View({ }) {

    try{

        const job =  await  queueView.add({ });  

        const { statuscode, data, message } = await job.finished(); 
        
        console.log({ statuscode, data, message });

        return { statuscode, data, message }

    }catch(error){

        console.log(error)

    }

}

async function main(){
    // await Create({ name: 'Javier', age: 29, color: 'Azul' });

    // await Delete({ id: 1 } );

    // await Update({  age: 50, name: 'Nelson', color: 'Negro', id: 1 });

     // await View({ });

    await FindOne( { id: 3 });
}

// main();

module.exports = {
    Create,
    Delete,
    Update,
    View,
    FindOne
}