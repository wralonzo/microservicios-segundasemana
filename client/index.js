const { io }  = require('socket.io-client');

const socket = io('http://localhost', { port: 80 });

async function main(){

    try{

        setTimeout(() => { console.log(socket.id) }, 500);

        socket.on('res:microservice:view', ({ statuscode, data, message }) => {

            console.log('res:microservice:view', { statuscode, data, message });
            
        });
        
        socket.on('res:microservice:create', ({ statuscode, data, message }) => {

            console.log('res:microservice:create', { statuscode, data, message });
        });

        socket.on('res:microservice:delete', ({ statuscode, data, message }) => {

            console.log('res:microservice:delete', { statuscode, data, message });
        });

        socket.on('res:microservice:update', ({ statuscode, data, message }) => {

            console.log('res:microservice:update', { statuscode, data, message });
        });

        socket.on('res:microservice:findOne', ({ statuscode, data, message }) => {

            console.log('res:microservice:findOne', { statuscode, data, message });
        });

        // setInterval(() => { socket.emit('req:microservice:view', ({ })) }, 500);

        setTimeout(() => {

            // socket.emit('req:microservice:create', ({ age: 27, color:'Purple', name:'Kike' }));
            // socket.emit('req:microservice:view', ({ }));
            // socket.emit('req:microservice:delete', ({ id: 1 }));
            // socket.emit('req:microservice:update', ({ age: 50, color:'Amarillo', name:'Random', id: 5 }));
             socket.emit('req:microservice:findOne', ({ id: 6 }));


        }, 300)

    }
    catch(error){
        console.error(error);
    }
}

main();