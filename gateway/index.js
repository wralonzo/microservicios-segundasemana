const express = require('express');

const http = require('http');

const app = express();

const server = http.createServer(app);

const { Server } = require('socket.io');

const io = new Server(server);

const api = require('../api');

server.listen(80, () => {

    console.log('Server inicializado');

    io.on('connection', socket => {
       
        console.log('Nueva conexion', socket.id);

        socket.on('req:microservice:view', async({  }) => {

           try{

            const { statuscode, data, message } =  await api.View({ });

            return io.to(socket.id).emit('res:microservice:view', { statuscode, data, message });

           }
           catch(error){
               console.error(error);
           }
        });

        socket.on('req:microservice:create', async({ name, age, color }) => {

            try{

                console.log('He sido llamado CREATE', { name, age, color });

                const { statuscode, data, message } =  await api.Create({ name, age, color });

                return io.to(socket.id).emit('res:microservice:create', { statuscode, data, message });

            } 
            catch(error){
                console.error(error);
            }
        });

        socket.on('req:microservice:delete', async({ id }) => {
            try{

                console.log('He sido llamado desde DELETE', { id });

                const { statuscode, data, message } =  await api.Delete({ id});

                return io.to(socket.id).emit('res:microservice:delete', { statuscode, data, message });

            } 
            catch(error){
                console.error(error);
            }
            });

        socket.on('req:microservice:update', async({ name, age, color, id }) => {

            try{

                console.log('He sido llamado Update', { name, age, color, id });

                const { statuscode, data, message } =  await api.Update({ name, age, color, id });

                return io.to(socket.id).emit('res:microservice:update', { statuscode, data, message });

            } 
            catch(error){
                console.error(error);
            }
        });

        socket.on('req:microservice:findOne', async({ id }) => {

            try{
                console.log('He sido llamado FINDONE', { id });

                const { statuscode, data, message } =  await api.FindOne({ id});

                return io.to(socket.id).emit('res:microservice:findOne', { statuscode, data, message });

            } 
            catch(error){
                console.error(error);
            }
        });


    });

});

